package epam.task.arrays;

import org.apache.commons.lang3.ArrayUtils;

import java.util.*;
import java.util.stream.Collectors;

public class LogicalTasks {

    private int[] firstArr = new int[30];
    private int[] secondArr = new int[30];
    private int[] thirdArr = new int[30];
    private int[] differArray;

    public LogicalTasks() {
        addDataToArray(firstArr);
        addDataToArray(secondArr);
    }

    private void addDataToArray(int[] array){
        for(int i = 0; i< array.length-1; i++){
            array[i] = getRandomCount();
        }
    }

    private int getRandomCount(){
        return new Random().nextInt(45);
    }

    public void checkSameData(){
        int third_index = 0;
        ArrayList<Integer> arr = new ArrayList<>();
        for(int i: firstArr)
            for(int j: secondArr)
                if(!arr.contains(i) && i == j)
                    arr.add(i);
        arr.sort(null);
        thirdArr = new int[arr.size()];
        for(int i: arr){
            thirdArr[third_index] = i;
            third_index++;
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void checkDifferentData() {
        ArrayList<Integer> arr = new ArrayList<>();
        for(Integer count : firstArr) {
            arr.add(count);
            arr.sort(null);
        }
            for (int j : thirdArr){
                if (arr.contains(Integer.valueOf(j)))
                    //arr.remove(arr.indexOf(i));
                    arr.remove(Integer.valueOf(j));
            }

        for(int i: arr) System.out.print(i + " ");
        System.out.println();
    }

    public void checkMoreThreeTimesData() {
        int counter = 0;
        Map<Integer,Integer> map = new HashMap<>();
        ArrayList<Integer> arr = new ArrayList<>();
        for(Integer item : firstArr) {
            arr.add(item);
            arr.sort(null);
        }

        Map<String, Long> couterMap = arr.stream().collect(Collectors.groupingBy(e -> e.toString(),Collectors.counting()));

        /*Iterator it = map.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry item = (Map.Entry) it.next();

            if ((Integer) item.getValue() > 2){
                for (Integer j : arr){
                    if (arr.get(Integer.valueOf(j)).equals((Integer) item.getKey())){
                        arr.remove(Integer.valueOf(entry.getKey()));
                    }
                }
            }


            it.remove();
        }*/

        for (Map.Entry<String, Long> entry : couterMap.entrySet()) {
            if (entry.getValue() > 2){
                for (Integer j : arr){
                   
                   // if (arr.get(Integer.valueOf(j)).equals(Integer.valueOf(entry.getKey()))){
                        //    arr.remove(Integer.valueOf(entry.getKey()));
                    //}
                }
            }
        }
        for(int i: arr) System.out.print(i + " ");
        System.out.println();
    }

    public void getRepeatData() {
        Set <Integer> arr = new HashSet<>();
        for(Integer count : firstArr) {
            arr.add(count);
        }
        for(int i: arr) System.out.print(i + " ");
    }


    public int[] getFirstArr() {
        return firstArr;
    }

    public int[] getSecondArr() {
        return secondArr;
    }
}