package epam.task.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogicView {

    LogicalTasks logicalTasks;

    private static final Logger LOG = LogManager.getLogger(LogicalTasks.class);

    public LogicView() {
        logicalTasks = new LogicalTasks();
    }

    private void showArrData(int[] arr){
        for(int i = 0; i < arr.length; i++){

            System.out.print(arr[i] + " ,");
        }
        System.out.println();
    }

    public void callArrays(){
        //LOG.info("First arr :");
        System.out.println("Arrays:");
        showArrData(logicalTasks.getFirstArr());
       // LOG.info("Second arr :");
        showArrData(logicalTasks.getSecondArr());
    }

    public void showSameData(){
        System.out.println("Same data array:");
        logicalTasks.checkSameData();
    }
    public void showDiffData(){
        System.out.println("Different data array:");
        logicalTasks.checkDifferentData();
    }

    public void showRepeatData(){
        System.out.println("Repeat data array:");
        logicalTasks.getRepeatData();
    }

    public void showMoreThreeTimesData(){
        System.out.println("More three two times data array:");
        logicalTasks.checkMoreThreeTimesData();
    }



}
