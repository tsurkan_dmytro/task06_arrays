package epam.task.arrays;

import epam.task.arrays.heroGame.Hero;

public class MainClass {
    public static void main(String[] args) {

        LogicView logicView = new LogicView();
        logicView.callArrays();
        logicView.showSameData();
        logicView.showDiffData();
        logicView.showMoreThreeTimesData();
        logicView.showRepeatData();

        Hero hero = new Hero();
        hero.createDors();
        hero.showDors();
        hero.countDeadDors();
        hero.countLiveDors();
    }
}
