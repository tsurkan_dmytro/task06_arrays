package epam.task.arrays.heroGame;

import java.util.Random;

public class Hero {

    private static final Random generator = new Random();
    private final int DORS_COUNT = 10;

    int health = 25;
    int dors[] = new int[DORS_COUNT];

    public void createDors(){
        for (int i = 0; i < dors.length; i++){
            int loop = getRandomInRange(0,1);
                if (loop == 0){
                    dors[i] = getRandomInRange(-100, -5);
                }else {
                    dors[i] = getRandomInRange(10, 80);
                }
        }
    }

    public static int getRandomInRange(int start, int end) {
        return start + generator.nextInt(end - start + 1);

    }

    public void  showDors() {
        System.out.println();
        System.out.print("Room dors power > ");
        for (int i : dors){
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void countDeadDors() {
        int count = 0;
        for (int i = 0; i < dors.length; i++) {
            if (dors[i] < 0) {
                count++;
            }
        }
        System.out.println("Dead dors > " + count);
    }

    public void countLiveDors() {
        int count = 0;
        System.out.print("Live dors number > ");
        for (int i = 0; i < dors.length; i++) {
            if (dors[i] > 0) {
                System.out.print(i + " ");
            }
        }
    }
}
